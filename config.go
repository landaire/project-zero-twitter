package main

import (
	"fmt"
	"os"
	"path"

	"strings"

	"github.com/kardianos/osext"
	"github.com/naoina/toml"
)

const (
	configFile = "config.toml"
)

type TwitterConfig struct {
	ConsumerToken  string `toml:"consumer_token"`
	ConsumerSecret string `toml:"consumer_secret"`
	AccessToken    string `toml:"access_token"`
	AccessSecret   string `toml:"access_secret"`
}

type Config struct {
	Twitter TwitterConfig `toml:"twitter"`
}

var (
	GlobalConfig *Config
)

func ParseConfig() *Config {
	var out Config

	ef, _ := osext.ExecutableFolder()

	file, err := os.Open(path.Join(ef, configFile))
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil
	}

	defer file.Close()

	decoder := toml.NewDecoder(file)

	if err := decoder.Decode(&out); err != nil {
		fmt.Fprintln(os.Stderr, err)
		return nil
	}

	GlobalConfig = &out

	return &out
}

func (this TwitterConfig) IsValid() bool {
	vals := []string{this.AccessSecret, this.AccessToken, this.ConsumerSecret, this.ConsumerToken}

	for _, val := range vals {
		if strings.TrimSpace(val) == "" {
			return false
		}
	}

	return true
}

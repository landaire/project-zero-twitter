package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"
	"encoding/json"

	"github.com/kardianos/osext"
	"github.com/mmcdole/gofeed"
)

const (
	BlogsFeed     = "https://googleprojectzero.blogspot.com/feeds/posts/default"
	IssueBoardURL = "https://bugs.chromium.org/prpc/monorail.Issues/ListIssues"
	IssueURLFmt   = "https://bugs.chromium.org/p/project-zero/issues/detail?id=%d"
	IssuePostData = `{"projectNames":["project-zero"],"query":"","cannedQuery":1,"groupBySpec":"","sortSpec":"","pagination":{"start":%d,"maxItems":500}}`
	SrfToken = "b3NAaSKjwhJb5UALKYvp8ToxNTgxNTM1NTcz"
	FetchRate     = 1 * time.Minute
	// XXX: This is changed daily by twitter but no need to make external requests
	// for this at this time
	ShortURLLength       = 23
	TweetLength          = 280
	MaxSummaryLength     = TweetLength - ShortURLLength - 1 // -1 for space
	TileClass            = "gridtile"
	PostedIssuesFileName = "posted_issues.txt"
	PostedBlogsFileName  = "posted_blogs.txt"
)

var (
	postedIssues = map[int]bool{}
	postedBlogs  = map[string]bool{}
	urlRegex     = regexp.MustCompile(`\?id=(\d+)`)
	idRegex      = regexp.MustCompile(`^\s*\d+\s*$`)
	issuesfname  = PostedIssuesFileName
	blogsfname   = PostedBlogsFileName
	dry          = false
)

func init() {
	ef, _ := osext.ExecutableFolder()
	issuesfname = path.Join(ef, issuesfname)
	blogsfname = path.Join(ef, blogsfname)
}

func main() {
	if len(os.Args) > 1 && os.Args[1] == "--dry" {
		dry = true
	}

	readPostedIssues()
	readPostedBlogs()
	ParseConfig()

	for {
		fmt.Println()
		fmt.Println("Checking for blog posts...")
		checkForBlogPosts()

		fmt.Println()
		fmt.Println("Checking for issues...")
		checkForIssues()
		// Fetch issues every hour
		<-time.After(FetchRate)
	}
}

func checkForBlogPosts() {
	fp := gofeed.NewParser()
	f, _ := fp.ParseURL(BlogsFeed)

	for _, item := range f.Items {
		if _, ok := postedBlogs[item.GUID]; ok {
			continue
		}

		postedBlogs[item.GUID] = true

		summary := item.Title
		if len(summary) > MaxSummaryLength {
			summary = summary[0 : MaxSummaryLength-1] // remove an additional character for the ellipses
			summary += "…"
		}

		tweet := fmt.Sprintf("%s %s", summary, item.Link)

		fmt.Println(tweet)
		if !dry {
			PostTweet(tweet)
		}
	}

	writePostedBlogs()
}

func getSrfToken() (string, error) {
	resp, err := http.Get("https://bugs.chromium.org/p/project-zero/issues/list?q=&can=1&mode=grid")
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	s := buf.String()

	re := regexp.MustCompile(`'token': '([^']+)',`)
	matches := re.FindAllStringSubmatch(s, 1)
	return matches[0][1], nil
}

func checkForIssues() {

	srfToken, err := getSrfToken()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		<-time.After(FetchRate)
		return
	}

	totalItems := 1
	postedItems := 0
	for postedItems <= totalItems {
		body := strings.NewReader(fmt.Sprintf(IssuePostData, postedItems))
		req, err := http.NewRequest("POST", IssueBoardURL, body)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			<-time.After(FetchRate)
			return
		}

		req.Header.Set("Accept", "application/json")
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-Xsrf-Token", srfToken)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			<-time.After(FetchRate)
			return
		}
		defer resp.Body.Close()

		// Read the first 4 bytes/first line to get rid of their anti-XSS code
		count, err := resp.Body.Read([]byte{0,0,0,0})
		if count != 4 {
			fmt.Fprintln(os.Stderr, "Could not read at least 4 bytes from response")
			<-time.After(FetchRate)
			return
		}
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			<-time.After(FetchRate)
			return
		}

		responseRawData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			<-time.After(FetchRate)
			return
		}

		var responseData MonorailIssuesResponse
		err = json.Unmarshal(responseRawData, &responseData)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			<-time.After(FetchRate)
			return
		}

		totalItems = responseData.TotalResults
		for _, issue := range responseData.Issues {
			postedItems += 1

			id := issue.LocalID
			summary := issue.Summary

			if posted, ok := postedIssues[id]; posted && ok {
				continue
			}

			if len(summary) > MaxSummaryLength {
				summary = summary[0 : MaxSummaryLength-1] // remove an additional character for the ellipses
				summary += "…"
			}

			tweet := fmt.Sprintf("%s %s", summary, fmt.Sprintf(IssueURLFmt, id))

			fmt.Println(tweet)
			if !dry {
				PostTweet(tweet)
			}

			postedIssues[id] = true
		}

		writePostedIssues()
	}
}

func writePostedIssues() {
	file, err := os.Create(issuesfname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	for id, _ := range postedIssues {
		fmt.Fprintf(file, "%d,", id)
	}

	file.Close()
}

func writePostedBlogs() {
	file, err := os.Create(blogsfname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	for id, _ := range postedBlogs {
		if strings.TrimSpace(id) == "" {
			continue
		}

		fmt.Fprintf(file, "%s~", id)
	}

	file.Close()
}

func readPostedIssues() {
	file, err := os.Open(issuesfname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	text, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	for _, id := range strings.Split(string(text), ",") {
		if strings.TrimSpace(id) == "" {
			continue
		}

		idAsInt, _ := strconv.Atoi(id)
		postedIssues[idAsInt] = true
	}

	file.Close()
}

func readPostedBlogs() {
	file, err := os.Open(blogsfname)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	text, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	for _, id := range strings.Split(string(text), "~") {
		if strings.TrimSpace(id) == "" {
			continue
		}

		postedBlogs[id] = true
	}

	file.Close()
}

type MonorailIssuesResponse struct {
	TotalResults int `json:"totalResults"`
	Issues       []struct {
		OpenedTimestamp        int    `json:"openedTimestamp"`
		LocalID                int    `json:"localId"`
		ProjectName            string `json:"projectName"`
		OwnerModifiedTimestamp int    `json:"ownerModifiedTimestamp,omitempty"`
		StarCount              int    `json:"starCount,omitempty"`
		ModifiedTimestamp      int    `json:"modifiedTimestamp"`
		Summary                string `json:"summary"`
		OwnerRef               struct {
			DisplayName string `json:"displayName"`
			UserID      string `json:"userId"`
		} `json:"ownerRef"`
		LabelRefs []struct {
			Label string `json:"label"`
		} `json:"labelRefs"`
		CcRefs []struct {
			DisplayName string `json:"displayName"`
			IsDerived   bool   `json:"isDerived"`
			UserID      string `json:"userId"`
		} `json:"ccRefs,omitempty"`
		ClosedTimestamp            int `json:"closedTimestamp,omitempty"`
		StatusModifiedTimestamp    int `json:"statusModifiedTimestamp"`
		ComponentModifiedTimestamp int `json:"componentModifiedTimestamp,omitempty"`
		ReporterRef                struct {
			DisplayName string `json:"displayName"`
			UserID      string `json:"userId"`
		} `json:"reporterRef"`
		AttachmentCount    int `json:"attachmentCount,omitempty"`
		MergedIntoIssueRef struct {
			ProjectName string `json:"projectName"`
			LocalID     int    `json:"localId"`
		} `json:"mergedIntoIssueRef,omitempty"`
		BlockingIssueRefs []struct {
			ProjectName string `json:"projectName"`
			LocalID     int    `json:"localId"`
		} `json:"blockingIssueRefs,omitempty"`
		BlockedOnIssueRefs []struct {
			ProjectName string `json:"projectName"`
			LocalID     int    `json:"localId"`
		} `json:"blockedOnIssueRefs,omitempty"`
		StatusRef struct {
			Status    string `json:"status"`
			MeansOpen bool   `json:"meansOpen"`
		} `json:"statusRef,omitempty"`
	} `json:"issues"`
}

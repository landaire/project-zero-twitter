package main

import (
	"fmt"
	"os"

	"github.com/ChimeraCoder/anaconda"
)

const (
	TweetMaxLength = 140
)

var (
	twitterClient *anaconda.TwitterApi
)

func PostTweet(t string) {
	if !GlobalConfig.Twitter.IsValid() {
		return
	}

	if twitterClient == nil {
		twitterConfig := GlobalConfig.Twitter

		anaconda.SetConsumerKey(twitterConfig.ConsumerToken)
		anaconda.SetConsumerSecret(twitterConfig.ConsumerSecret)

		twitterClient = anaconda.NewTwitterApi(twitterConfig.AccessToken, twitterConfig.AccessSecret)
	}

	_, err := twitterClient.PostTweet(t, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	fmt.Println("Tweet posted")
}
